includes library Adafruit_CircuitPython_MPR121 and adafruit_bus_device
from
https://github.com/adafruit/Adafruit_CircuitPython_MPR121
main branch

lib files compiled to .mpy for circuitpython 7.x version only.

# Use:

* Probably have to delete everything from CIRCUITPYTHON/lib
* You may have to clean out hidden files. The mac has a nasty tendency to create extra files, and the Trinket M0 has only 48k of space! (notably ._* files)
* Copy the *contents* of the `lib` to folder to `CIRCUITPYTHON/lib`
* Copy `code.py` to CIRCUITPYTHON


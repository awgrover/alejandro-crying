"""
    Detect if 2 are touched, start/stop/keep crying.
    Has a minium cry time, and a maximum.
    Assumes a pwm drive pin for the pump.  

    Was designed for the trinket M0
"""
import gc
print("0 U{:}/F{:}".format(gc.mem_alloc()/1000.0, gc.mem_free()/1000.0))

import time
import board
import busio
import digitalio
import pwmio
import adafruit_mpr121
from every.every import Every
from every.every import Timer

# debug: how much memory avail?
print("0 U{:}/F{:}".format(gc.mem_alloc()/1000.0, gc.mem_free()/1000.0))

# motor setup
motor = pwmio.PWMOut(board.D3)
motor.duty_cycle = 0 # don't run yet
motor_speed = int(65535 * 0.8) # target speed when running: 0..65535 (really 0..255 scaled)

# mpr121 setup
i2c = busio.I2C(board.SCL, board.SDA) # "SDA or SCL needs a pull up" means not connected
mpr121 = adafruit_mpr121.MPR121(i2c)

# for heartbeat
red_led = digitalio.DigitalInOut(board.LED)
red_led.direction = digitalio.Direction.OUTPUT
heartbeat = Every(0.500)

# Timers
check_touch = Every(0.100)
min_duration = Timer(1) # minimum cry duration: quick touch
max_duration = Timer(10) # maximum cry duration

# Globals
last_touched = [False,False] # keep track of the last state (for "did it change")
held_count = 0 # when 2, then both held

# setup
print("Start")

# functions

def start_crying():
    # how to start crying
    print("Start crying ", time.monotonic());

    # run motor
    motor.duty_cycle = motor_speed

    # start timers
    min_duration.start()
    max_duration.start()
    
def stop_crying():
    # how to stop/interrupt crying
    # We don't stop immediately,
    # nor do we need to adjust timers
    print("Stop crying ", time.monotonic());

    # To continue crying after "release", 
    # add another timer like min_duration and start it here

def keep_crying():
    # always called! even if both hands are NOT held
    # This is where we actually stop the motors (after checking timers)
    # so: have various timers run out

    # should we be running (really, "are both hands held")
    should_be_running = held_count == 2

    # don't run longer than max_duration
    if should_be_running and max_duration():
        print("Max")
        motor.duty_cycle = 0

    # run at least min_duration even if released
    if not should_be_running and min_duration():
        # the minimum run duration has passed
        motor.duty_cycle = 0

# loop
while True:
    # heartbeat
    if heartbeat():
        red_led.value = not red_led.value
            
    new_touch = False # when True, a new hold happened
    released = False # when True, a new release happened

    # it's "polite" to not check every single time
    # (doesn't really matter in this code)
    if (check_touch()):
        held_count = 0 # count each current touch (till 2)

        current_touch = [False,False]

        # i goes 0,1 (i.e. 2 times)
        for i in range(2):
            # Get touch [i]
            current_touch[i] = mpr121[i].value

            # if it *is* touched and *wasnt* touched before, count & note
            if ( current_touch[i] ):
                # our logic is simple: we want 2 touches, so just count them
                held_count += 1

                # a change is how we decide to call start_crying/stop_crying
                if ( not last_touched[i] ):
                    new_touch = True
                    print(i," touched")

            # if it *was* touched and now *isnt*, note it
            elif ( last_touched[i] ):
                released = True
                print(i," released")

        # remember our state
        for i in range(2):
            last_touched[i] = current_touch[i]

        """
          # debugging info, what
          Serial.print("\t\t\t\t\t\t\t\t\t\t\t\t\t 0x") Serial.println(cap.touched(), HEX)
          Serial.print("Filt: ")
          for (uint8_t i = 0 i < 12 i++):
            Serial.print(cap.filteredData(i)) Serial.print("\t")

          Serial.println()
          Serial.print("Base: ")
          for (uint8_t i = 0 i < 12 i++):
            Serial.print(cap.baselineData(i)) Serial.print("\t")

          Serial.println()
        """

        # Decide which function to call
        if (held_count == 2):
            if (new_touch):
                start_crying()
        elif (released):
            stop_crying()
        
        # end check_touch()

    # We always check for keep'ing-crying
    keep_crying()        
